#  This script only works in the ./djgpp directory.

export HOME=.
export PATH_SEPARATOR=:
export PATH_EXPAND=y
export TEST_FINDS_EXE=y
export LD=ld
export NM=nm
export LN_S="cp -p"
export GMSGFMT=${GMSGFMT='/dev/env/DJDIR/bin/msgfmt'}
export MSGFMT=${MSGFMT='/dev/env/DJDIR/bin/msgfmt'}
export MSGMERGE=${MSGMERGE='/dev/env/DJDIR/bin/msgmerge'}
export XGETTEXT=${XGETTEXT='/dev/env/DJDIR/bin/xgettext'}
export ac_cv_path_AWK=${AWK='/dev/env/DJDIR/bin/gawk'}
export ac_cv_path_EMACS=${EMACS='/dev/env/DJDIR/gnu/emacs/bin/emacs'}
export ac_cv_path_M4=${M4='/dev/env/DJDIR/bin/m4'}
export ac_cv_path_GREP=${GREP='/dev/env/DJDIR/bin/grep'}
export ac_cv_path_EGREP=${EGREP='/dev/env/DJDIR/bin/egrep'}
export ac_cv_path_FGREP=${FGREP='/dev/env/DJDIR/bin/fgrep'}
export ac_cv_path_SED=${SED='/dev/env/DJDIR/bin/sed'}
export ac_cv_path_MAKEINFO=${MAKEINFO='/dev/env/DJDIR/bin/makeinfo'}
export ac_cv_path_INSTALL_INFO=${INSTALL_INFO='/dev/env/DJDIR/bin/install-info'}
export ac_cv_path_ROFF=${ROFF='/dev/env/DJDIR/bin/groff'}
export ac_cv_path_GROFF=${GROFF='/dev/env/DJDIR/bin/groff'}
export ac_cv_path_NROFF=${NROFF='/dev/env/DJDIR/bin/nroff'}
export ac_cv_path_PERL=${PERL='/dev/env/DJDIR/bin/perl'}
export ac_cv_path_mkdir=${MKDIR_P='/dev/env/DJDIR/bin/mkdir -p'}
export ac_cv_path_RM=${RM='/dev/env/DJDIR/bin/rm'}
export ac_cv_path_MV=${MV='/dev/env/DJDIR/bin/mv'}
export ac_cv_path_TAR=${TAR='/dev/env/DJDIR/bin/tar'}
export ac_cv_path_PR_PROGRAM=${PR='/dev/env/DJDIR/bin/pr'}
export ac_cv_path_ed_PROGRAM=${ED='/dev/env/DJDIR/bin/ed'}
export ac_cv_path_TEXI2DVI=${TEXI2DVI='texi2dvi'}
export ac_cv_path_TEXI2PDF=${TEXI2PDF='texi2pdf'}
export ac_cv_path_DVIPS=${DVIPS='dvips'}
export ac_cv_path_PS2PDF=${PS2PDF='ps2pdf'}
export ac_cv_path_TEST_SHELL=${TEST_SHELL='/dev/env/DJDIR/bin/bash'}
export ac_cv_path_MKTEMP=${MKTEMP='/dev/env/DJDIR/bin/mktemp'}
export ac_cv_prog_LN_S="cp -p"
export ac_cv_prog_AWK=${AWK='/dev/env/DJDIR/bin/gawk'}
export ac_cv_prog_EMACS=${EMACS='/dev/env/DJDIR/gnu/emacs/bin/emacs'}
export ac_cv_prog_M4=${M4='/dev/env/DJDIR/bin/m4'}
export ac_cv_prog_GREP=${GREP='/dev/env/DJDIR/bin/grep'}
export ac_cv_prog_EGREP=${EGREP='/dev/env/DJDIR/bin/egrep'}
export ac_cv_prog_FGREP=${FGREP='/dev/env/DJDIR/bin/fgrep'}
export ac_cv_prog_SED=${SED='/dev/env/DJDIR/bin/sed'}
export ac_cv_prog_MAKEINFO=${MAKEINFO='/dev/env/DJDIR/bin/makeinfo'}
export ac_cv_prog_INSTALL_INFO=${INSTALL_INFO='/dev/env/DJDIR/bin/install-info'}
export ac_cv_prog_ROFF=${ROFF='/dev/env/DJDIR/bin/groff'}
export ac_cv_prog_GROFF=${GROFF='/dev/env/DJDIR/bin/groff'}
export ac_cv_prog_NROFF=${NROFF='/dev/env/DJDIR/bin/nroff'}
export ac_cv_prog_PERL=${PERL='/dev/env/DJDIR/bin/perl'}
export ac_cv_prog_mkdir=${MKDIR_P='/dev/env/DJDIR/bin/mkdir -p'}
export ac_cv_prog_RM=${RM='/dev/env/DJDIR/bin/rm'}
export ac_cv_prog_MV=${MV='/dev/env/DJDIR/bin/mv'}
export ac_cv_prog_CAT=${CAT='/dev/env/DJDIR/bin/cat'}
export ac_cv_prog_SORT=${SORT='/dev/env/DJDIR/bin/sort'}
export ac_cv_prog_TAR=${TAR='/dev/env/DJDIR/bin/tar'}
export ac_cv_prog_PR_PROGRAM=${PR='/dev/env/DJDIR/bin/pr'}
export ac_cv_prog_ed_PROGRAM=${ED='/dev/env/DJDIR/bin/ed'}
export ac_cv_prog_TEXI2DVI=${TEXI2DVI='texi2dvi'}
export ac_cv_prog_TEXI2PDF=${TEXI2PDF='texi2pdf'}
export ac_cv_prog_DVIPS=${DVIPS='dvips'}
export ac_cv_prog_PS2PDF=${PS2PDF='ps2pdf'}
export ac_cv_prog_TEST_SHELL=${TEST_SHELL='/dev/env/DJDIR/bin/bash'}
export ac_cv_prog_MKTEMP=${MKTEMP='/dev/env/DJDIR/bin/mktemp'}
export ac_cv_func_fork=no
export ac_cv_func_mkfifo=no
export ac_cv_func_mknod=no
export ac_cv_func_mmap=no
export ac_cv_func_vfork=no

# Do not allow that the BFD's configure script determine the
# host dependant file_ptr a.k.a. off_t type as BFD_HOST_64_BIT.
# This is the case if ftello64 and fseeko64 are found.  But DJGPP
# does not provide the full set of 64 bit file functions required
# for a working 64 bit BFD.
export ac_cv_func_fseeko64=${ac_cv_func_fseeko64=no}
export ac_cv_func_ftello64=${ac_cv_func_ftello64=no}
export ac_cv_have_decl_fseeko64=${ac_cv_have_decl_fseeko64=no}
export ac_cv_have_decl_ftello64=${ac_cv_have_decl_ftello64=no}

# Ensure that always old GNU extern inline semantics is used
# (aka -fgnu89-inline) even if ISO C99 semantics has been specified.
case $(gcc --version 2>/dev/null | sed "/^.* \([1-9]\+\.[0-9]\+[.0-9]*\).*$/!d;s/^.* \([1-9]\+\.[0-9]\+[.0-9]*\).*$/\1/") in
[1-3].*|4.[0-1][.0-9]* )  export CFLAGS=${CFLAGS='-g2 -O2 -std=gnu99 -march=i386 -mtune=i586'};;
* )                       export CFLAGS=${CFLAGS='-g2 -O2 -fgnu89-inline -march=i386 -mtune=i586'};;
esac

# DJGPP's implementation of printf survives out-of-memory conditions.
export gl_cv_func_printf_enomem='yes'

# Enable libiberty installation.
# Passing --enable-install-libiberty to the toplovel configure seems not to be enough.
export enable_install_libiberty=yes

# Select NLS support.
# NLS_SUPPORT='--enable-nls'
 NLS_SUPPORT='--disable-nls'

if [ "x${NLS_SUPPORT}" = "x--enable-nls" ]; then
  rm -vf ../bfd/po/*gmo
  rm -vf ../bfd/po/*pot
  rm -vf ../binutils/po/*gmo
  rm -vf ../binutils/po/*pot
  rm -vf ../gas/po/*gmo
  rm -vf ../gas/po/*pot
  rm -vf ../gold/po/*gmo
  rm -vf ../gold/po/*pot
  rm -vf ../gprof/po/*gmo
  rm -vf ../gprof/po/*pot
  rm -vf ../ld/po/*gmo
  rm -vf ../ld/po/*pot
  rm -vf ../opcodes/po/*gmo
  rm -vf ../opcodes/po/*pot
  rm -vf ../bfd/po/*gmo
  rm -vf ../bfd/po/*pot
fi


#
#  1: Adjust the configure scripts.
#

cat > script.sed << EOF
# For some reason the function does not work with bash 2.05b.
/^func_lalib_p/,/^}$/ {
  /test/ i\\
    case \$1 in\\
    *.la | *.lo)
  /GREP/ {
    s/$/;;/
    a\\
    *) false;;\\
    esac
  }
}

# Use func_lalib_p instead of func_lalib_unsafe_p.
/func_lalib_unsafe_p[ 	][^(]/ s/_unsafe//

# Adjust temp directory.
/{TMPDIR-\/tmp}/ s|{TMPDIR-/tmp}|{TMPDIR-\${TMP-\${TEMP-.}}}|

# Remove -lc reference from the dependency_libs= line in .la files.
# This is unnecessary when linking with static  labraries and causes
# multiple symbol definitions that cannot be resolved when using DXE3 modules.
/^# Libraries that this one depends upon.$/,/^# Directory that this library needs to be installed in:$/ {
  /^# Directory that this library needs to be installed in:$/ {
    n
    a\\
	  sed "/^dependency_libs=/ s|[ 	]\\\\{1,\\\\}-lc| |"  \$output > fixed.sed && cp -vf fixed.sed \$output
  }
}

# Supress makeinfo test.  DJGPP does not provide any other port than 4.13.
/# For an installed makeinfo, we require it to be from texinfo 4.7 or/,/;;/ {
  /MAKEINFO.*makeinfo/ s/MAKEINFO/IGNORE_&/
}

# We always use _deps and _libs instead of .deps and .libs, because
# the latter is an invalid name on 8+3 MS-DOS filesystem.  This makes
# the generated Makefiles good for every DJGPP installation, not only
# the one where the package was configured (which could happen to be
# a Windows box, where leading dots in file names are allowed).
s,\.deps,_deps,g
s,\.libs,_libs,g
/^rmdir[ 	]*\.tst/ i\\
am__leading_dot=_

# Autoconf 2.63b produces if clauses that are enclosed in \`
# so we cannot use \` to replace parenthesized commands.
# This case must be treated before the parenthesized commands
# are replaced by \`.
/.*\`if[ 	](/ {
  s/(/"/
  s/)/"/
}

# Replace (command) > /dev/null with \`command > /dev/null\`, since
# parenthesized commands always return zero status in the ported Bash,
# even if the named command doesn't exist
/if ([^|;\`]*null/{
  s,(,\`,
  s,\\([^ 	)]\\)),\\1,
  /null[ 	]*2>&1/ s,2>&1,&\`,
  /null.*null/ s,null.*null,&\`,
  /null.*null/ !{
    /null[ 	]*2>&1/ !s,null,&\`,
  }
}

# DOS-style absolute file names should be supported as well
/\*) top_srcdir=/s,/\*,[\\\\\\\\/]* | ?:[\\\\\\\\/]*,

# The following two items are changes needed for configuring
# and compiling across partitions.
# 1) The given srcdir value is always translated from the
#    "x:" syntax into "/dev/x" syntax while we run configure.
/^[ 	]*-srcdir=\\*.*$/ a\\
    ac_optarg=\`echo "\$ac_optarg" | sed "s,^\\\\([A-Za-z]\\\\):,/dev/\\\\1,"\`
/set X \`ls -Lt \\\$srcdir/ i\\
   if \`echo \$srcdir | grep "^/dev/" - > /dev/null\`; then\\
     srcdir=\`echo "\$srcdir" | sed -e "s%^/dev/%%" -e "s%/%:/%"\`\\
   fi

# Autoconf 2.52e generated configure scripts
# write absolute paths into Makefiles making
# them useless for DJGPP installations for which
# the package has not been configured for.
/am_missing_run=/,/^$/ {
  /^fi$/ a\\
am_missing_run=\`echo "\$am_missing_run" | sed 's%/dev.*/@FILE_NAME@[-_0-9]\\\\{1,1\\\\}[-.0-9A-z+]*%\${top_srcdir}%;s%.:.*/@FILE_NAME@[-_0-9]\\\\{1,1\\\\}[-.0-9A-z+]*%\${top_srcdir}%'\`
}
/^[	 ]*install_sh=/,/^$/ {
  /^fi$/ a\\
install_sh=\`echo "\$install_sh" | sed 's%/dev.*/@FILE_NAME@[-_0-9]\\\\{1,1\\\\}[-.0-9A-z+]*%\${top_srcdir}%;s%.:.*/@FILE_NAME@[-_0-9]\\\\{1,1\\\\}[-.0-9A-z+]*%\${top_srcdir}%'\`
}

# Only if both builddir and srcdir are on the same
# partition the absolute paths are converted to
# relative paths so that the produced makefiles are
# good for all installations but not only for the one
# where it was configured.
/^# If the template does not know about datarootdir, expand it.$/ i\\
# DJGPP specific.\\
# Autoconf generated configure scripts write\\
# absolute paths into Makefiles making them\\
# useless for DJGPP installations for which\\
# the package has not been configured for.\\
djgpp_ac_abs_top_builddir_drive=\\\$(echo \${ac_abs_top_builddir} | sed -e 's%:[/\\\\\\\\]*.*$%%;s%^/dev/\\\\([A-z]\\\\)/.*$%\\\\1%')\\
djgpp_ac_abs_top_srcdir_drive=\\\$(echo \${ac_abs_top_srcdir} | sed -e 's%:[/\\\\\\\\]*.*$%%;s%^/dev/\\\\([A-z]\\\\)/.*$%\\\\1%')\\
if [ "x\\\${ac_abs_top_srcdir}" == "x\\\${ac_top_srcdir}" ]; then\\
  paths_are_absolute=yes\\
else\\
  paths_are_absolute=no\\
fi\\
\\
if [ "x\\\${djgpp_ac_abs_top_builddir_drive}" == "x\\\${djgpp_ac_abs_top_srcdir_drive}" ]; then\\
  # builddir and srcdir are on the same partition.\\
\\
  # Convert absolute buildir paths to relative ones.\\
  paths_to_be_adjusted="ac_abs_builddir ac_abs_top_builddir"\\
  for path in \\\${paths_to_be_adjusted}; do\\
    eval _path=\\\\\\\${\$path}\\
    eval djgpp_\\\${path}=\$(echo \\\${_path} | sed -e "s%\\\${ac_abs_top_builddir}%\\\${ac_builddir}%")\\
  done\\
\\
  if [ "x\\\${paths_are_absolute}" == "xyes" ]; then\\
    # Convert absolute srcdir paths to relative ones.\\
    djgpp_relative_prefix=\$(echo \\\${ac_abs_builddir} | sed -e 's%^[A-z]:%%;s%^/dev/[A-z]%%;s%^%.%;s%[/\\\\\\\\][A-z0-9._+-]\\\\+%/..%g;s%[/\\\\\\\\]$%%')\\
    paths_to_be_adjusted="ac_srcdir ac_abs_srcdir ac_top_srcdir ac_abs_top_srcdir"\\
    for path in \\\${paths_to_be_adjusted}; do\\
      eval _path=\\\\\\\${\$path}\\
      eval djgpp_\\\${path}=\$(echo \\\${_path} | sed -e "s%^[A-z]:%%;s%^/dev/[A-z]%%;s%^%\\\${djgpp_relative_prefix}%")\\
    done\\
  else\\
    djgpp_ac_srcdir="\\\${ac_srcdir}"\\
    djgpp_ac_abs_srcdir="\\\${ac_srcdir}"\\
    djgpp_ac_top_srcdir="\\\${ac_top_srcdir}"\\
    djgpp_ac_abs_top_srcdir="\\\${ac_top_srcdir}"\\
  fi\\
else\\
  # builddir and srcdir are on different partitions.\\
\\
  paths_to_be_adjusted="ac_abs_builddir ac_abs_top_builddir"\\
  for path in \\\${paths_to_be_adjusted}; do\\
    eval djgpp_\\\${path}=\\\\\\\${\$path}\\
  done\\
\\
  paths_to_be_adjusted="ac_srcdir ac_abs_srcdir ac_top_srcdir ac_abs_top_srcdir"\\
  for path in \\\${paths_to_be_adjusted}; do\\
    eval djgpp_\\\${path}=\\\\\\\${\$path}\\
  done\\
fi\\


/^s|@configure_input@|\\\$ac_sed_conf_input|;t t$/,/^\\\$ac_datarootdir_hack$/ {
  s/\\\$ac_srcdir/\\\$djgpp_ac_srcdir/
  s/\\\$ac_abs_srcdir/\\\$djgpp_ac_abs_srcdir/
  s/\\\$ac_top_srcdir/\\\$djgpp_ac_top_srcdir/
  s/\\\$ac_abs_top_srcdir/\\\$djgpp_ac_abs_top_srcdir/
  s/\\\$ac_abs_builddir/\\\$djgpp_ac_abs_builddir/
  s/\\\$ac_abs_top_builddir/\\\$djgpp_ac_abs_top_builddir/
}

# Add DJGPP version information.
/^#define VERSION/ s/\\\$VERSION/&  (DJGPP port (r1))/

# We need makeinfo to make the html formated docs.
/\\\$am_missing_run[ 	]*makeinfo/ s,\\\$am_missing_run,,

# The path to the FORTRAN compiler and libraries
# shall contain no absolute path reference so it
# will be good for all djgpp installations.
/^FLIBS="\\\$ac_cv_flibs"/ i\\
ac_djgpp_path=\`echo "\$DJDIR" | sed 's%\\\\\\\\\\\\%/%g' | tr \$as_cr_LETTERS \$as_cr_letters\`\\
ac_cv_flibs=\`echo "\$ac_cv_flibs" | sed "s%-L$ac_djgpp_path%-L/dev/env/DJDIR%g"\`

# Do not split info output.
/^MAKEINFO=.*$/s:=.*$:&" --no-split":

# The CR test for awk does not work for DJGPP.
/^ac_cs_awk_cr=/,/^$/ {
  /^fi$/ a\\
ac_cs_awk_cr=\$ac_cr
}

# AWK program above erroneously prepends '/' to C:/dos/paths
/# AWK program above erroneously prepends/,/esac/ {
  s/mingw/*djgpp* | mingw/
}

# Force the test for 'ln -s' to report 'cp -pf'.
/as_ln_s='ln -s'/ s/ln -s/cp -pf/

##LIBTOOL="\${CONFIG_SHELL-\$SHELL} "'\$(top_builddir)/libtool'
# Set the right shell for libtool
/^LIBTOOL=.*libtool'$/ s/".*"/'\$(SHELL) '/

# Adjust the config.site path for the case
# of recursive called configure scripts
/^if test "\$no_recursion" != yes; then/ a\\
  djgpp_config_site=\$CONFIG_SITE
/case \$srcdir in/,/esac/ {
  / # Relative name.$/ a\\
export CONFIG_SITE=\$ac_top_build_prefix\$djgpp_config_site
}

# DJGPP specific adjustments of the compile-time system search path for libraries.
/^[ 	]*lt_search_path_spec=.*-print-search-dirs/ s,\`\$, -e \\"s%[A-z]:[\\\\\\\\/]djgpp-[0-9].[0-9][0-9][\\\\\\\\/]%/dev/env/DJDIR/%g\\"&,

# Fix realpath check.  DJGPP always prepends a drive letter.
/checking whether realpath works/,/^_ACEOF$/ {
  /name && \\*name == '\\/'/ s/\\*name/name[2]/
}

# This works around a bug in DJGPP port of Bash 2.0x.
s|return \$ac_retval|(&)|g

# DJGPP port of Bash 2.04 doesn't like this redirection of stdin
/exec 7</s|7<&0 </dev/null||

# Remove LINGUAS from dependecy list in ./po/Makefile
/POMAKEFILEDEPS=/ s/POMAKEFILEDEPS LINGUAS/POMAKEFILEDEPS #LINGUAS/

# The dup2 check of autoconf will fail with SIGABRT due to implementations issues in DJGPP's version of dup2.
# To avoid this, it will not be allowed to use file descriptors greather than 255.
# This will be removed as soon as a fixed versions of DJGPP is released.
/checking whether dup2 works/,/^_ACEOF$/ {
  /dup2.*256);/d
}

# Convert am_aux_dir and abs_aux_dir into relative paths.
/^# Expand \$ac_aux_dir to an absolute path./ {
n
a\\
am_aux_dir=\${ac_aux_dir}
}

/abs_aux_dir=/ a\\
abs_aux_dir=\${ac_aux_dir}
EOF


for file in ../ltmain.sh ../configure ../bfd/configure ../binutils/configure ../gas/configure ../gprof/configure ../ld/configure ../opcodes/configure ../zlib/configure ../libctf/configure; do
  if test ! -f ${file}.orig; then
    cp -vf ${file} ${file}.orig
    touch ${file}.orig -r ${file}
  fi
  sed -f ./script.sed ${file}.orig > ./file.out
  update ./file.out ${file}
  touch ${file} -r ${file}.orig
done
rm -vf ./file.out ./script.sed



#
#  2: Adjust the Makefile.ins and other files.
#

cat > script.sed << EOF
# For html docs.
/^MAKEINFOHTML[ 	]*=/ s/$/ --no-split/
s/--split-size=5000000/--no-split/g

# Fixes for ./libiberty/Makefile.in
# ps support for libiberty docs.
/dvi-subdir[ 	]\\{1,\\}pdf-subdir/ s/dvi-subdir[ 	]\\{1,\\}pdf-subdir/& ps-subdir/

/^LIBIBERTY_PDFFILES[ 	]*=/ i\\
LIBIBERTY_PSFILES = libiberty.ps\\
\\
ps: \\\$(LIBIBERTY_PSFILES) ps-subdir\\


/^libiberty.pdf[ 	]*:/ i\\
libiberty.ps : ./libiberty.dvi \\\$(srcdir)/libiberty.texi \\\$(TEXISRC)\\
	dvips -o ./libiberty.ps ./libiberty.dvi\\

# Enable libiberty installation.
# Passing --enable-install-libiberty to the toplovel configure seems not to be enough.
s/@target_header_dir@/libiberty/

# Fixes for ./etc/Makefile.in.
/^HTMLFILES =.*configure.html$/ i\\
PSFILES = standards.ps configure.ps
/epstopdf/ s/[ 	]\\{1,\\}-outfile/ --outfile/

# Fixes for ./ld/Makefile.in.
/^install-exec-local[	 ]*:.*ld-new.*$/ {
  s/$/ install-data-local-djgpp/
i\\
install-data-local-djgpp:\\
	\\\$(mkinstalldirs) \\\$(DESTDIR)\\\$(scriptdir)\\
	for f in libnames.tab; do \\\\\\
	  \\\$(INSTALL_DATA) \\\$(top_srcdir)/\\\$\\\$f \\\$(DESTDIR)\\\$(scriptdir)/\\\$\\\$f ; \\\\\\
	done\\

}
EOF


for file in ./../bfd/Makefile.in ./../bfd/doc/Makefile.in ./../binutils/doc/Makefile.in ./../etc/Makefile.in ./../gas/doc/Makefile.in ./../gprof/Makefile.in ./../ld/Makefile.in ./../libiberty/Makefile.in ./../libctf/Makefile.in; do
  if test ! -f ${file}.orig; then
    cp -vf ${file} ${file}.orig
    touch ${file}.orig -r ${file}
  fi
  sed -f ./script.sed ${file}.orig > ./file.out
  update ./file.out ${file}
  touch ${file} -r ${file}.orig
done
rm -vf ./file.out ./script.sed

dtou ../ld/configure.ac
touch ../ld/configure.ac -r ../ld/configure.tgt


#
#  3: Configure and build the libs and programs.
#

touch start_build.txt

echo
echo Configuring the progs and libs.
echo See build_log.txt file for output.


echo Using: > build_log.txt
gcc --version >> build_log.txt
as --version >> build_log.txt
echo >> build_log.txt
ld --version >> build_log.txt
echo >> build_log.txt
echo >> build_log.txt
echo >> build_log.txt

echo Configuring the progs and libs for DJGPP. >> build_log.txt
echo >> build_log.txt

../configure  --prefix='/dev/env/DJDIR' --disable-dependency-tracking ${NLS_SUPPORT} \
              --with-mpc-include='/dev/env/DJDIR/include' --with-mpc-lib='/dev/env/DJDIR/lib' \
              --with-mpfr-include='/dev/env/DJDIR/include' --with-mpfr-lib='/dev/env/DJDIR/lib' \
              --with-gmp-include='/dev/env/DJDIR/include' --with-gmp-lib='/dev/env/DJDIR/lib' \
              --with-isl-include='/dev/env/DJDIR/include' --with-isl-lib='/dev/env/DJDIR/lib' \
              --enable-install-bfd --enable-install-libiberty \
              --enable-build-warnings=-Wimplicit,-Wcomment,-Wformat,-Wparentheses,-Wpointer-arith >> build_log.txt  2>&1

echo >> build_log.txt
echo ################################################################################ >> build_log.txt
echo >> build_log.txt


# Remove target alias from tooldir and scriptdir paths so
# that the linker scripts and binaries are installed in
# their DJGPP specific canonical places.
sed "/^.*tooldir = /s|/.*$||" ./Makefile > file.out
mv -vf ./file.out ./Makefile


echo
echo Building the progs and libs.
echo See build_log.txt file for output.
echo Building the progs and libs for DJGPP. >> build_log.txt
echo >> build_log.txt
make >> build_log.txt  2>&1

touch stop_build.txt
